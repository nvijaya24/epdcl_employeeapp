package com.epdcl.employeeapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by EPDCL on 21/09/2015.
 */

public class Signup extends Activity {

    EditText user_name ;
    EditText email_id ;
    EditText password,conf;
    TextView emailexists;
    Button signup,login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        signup = (Button)findViewById(R.id.button_signup);
        login = (Button)findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Signup.this, Signin.class);
                startActivity(i);
                finish();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_name = (EditText) findViewById(R.id.signup_fullnametext);
                email_id = (EditText) findViewById(R.id.emailtxt);
                password = (EditText) findViewById(R.id.signup_passwordtext);
                conf = (EditText) findViewById(R.id.signup_confirmpwdtext);
                emailexists = (TextView) findViewById(R.id.emailexists);
                Intent i = new Intent(Signup.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}
