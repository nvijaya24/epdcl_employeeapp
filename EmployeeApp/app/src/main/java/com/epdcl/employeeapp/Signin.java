package com.epdcl.employeeapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by EPDCL on 21/09/2015.
 */

public class Signin extends Activity {


    EditText email_id ,password;
    TextView emailexists ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        Button signin=(Button)findViewById(R.id.login);
        TextView signuplnk=(TextView)findViewById(R.id.signuplink);
        emailexists=  (TextView)findViewById(R.id.emailexists);
        TextView forgetpwd=(TextView)findViewById(R.id.forgetpwd);

        forgetpwd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Intent i = new Intent(Signin.this, Forgotpassword.class);
                    startActivity(i);
                    finish();
                }
            });

        signuplnk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i= new Intent(Signin.this,Signup.class);
                startActivity(i);
                finish();
            }
        });

        signin.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    email_id = (EditText)findViewById(R.id.signin_view_emailid);
                    password = (EditText)findViewById(R.id.signin_view_password);
                    if(email_id.equals("") || password.equals(""))
                    {
                        Toast.makeText(Signin.this, "Enter Email and Password", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Intent i = new Intent(Signin.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }

                }
            });
        }
}

