package com.epdcl.employeeapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by EPDCL on 21/09/2015.
 */

public class Forgotpassword extends Activity {
EditText email;
    Button btn;
    String pwrd;
    TextView login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgotpassword);
        email=(EditText)findViewById(R.id.email);
        btn=(Button)findViewById(R.id.recoverPswd);
        login=(TextView)findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Forgotpassword.this, Signin.class);
                startActivity(i);
                finish();
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pwrd=email.getText().toString().trim();

            }
        });
    }
}
