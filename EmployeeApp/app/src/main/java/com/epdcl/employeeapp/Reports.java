package com.epdcl.employeeapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by EPDCL on 18/09/2015.
 */
public class Reports extends Fragment {

    CategoriesListAdapterChat listAdapter;
    ArrayList<String> title=new ArrayList<String>();
    ListView lstvw;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView =inflater.inflate(R.layout.reports,container,false);
        lstvw=(ListView)rootView.findViewById(R.id.listvw);

        setListViewHeightBasedOnChildren(lstvw);

        title=new ArrayList<String>();
        title.add("Meter Observations");
        title.add("Meter Management");
        listAdapter = new CategoriesListAdapterChat(Reports.this.getActivity(),title);
        lstvw.setAdapter(listAdapter);

        lstvw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                if(position==0) {
                    Intent intent = new Intent(getActivity(), MetereObservations.class);
                    startActivity(intent);
                }
                else if(position==1) {
                    Intent intent = new Intent(getActivity(), MetereManagement.class);
                    startActivity(intent);
                }

            }
        });
        return rootView;
    }
    class CategoriesListAdapterChat extends BaseAdapter
    {
        private Context mContext;
        ArrayList<String> data;
        public CategoriesListAdapterChat(Context context,ArrayList<String> data)
        {
            super();
            mContext=context;
            this.data=data;
        }
        public int getCount()
        {
            return data.size();
        }
        // getView method is called for each item of ListView
        public View getView(final int position,  View view, ViewGroup parent)
        {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.reports_list, null);
            TextView title=(TextView)view.findViewById(R.id.itemtxt);
            title.setText(data.get(position));
            return view;
        }
        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
