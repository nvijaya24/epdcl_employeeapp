package com.epdcl.employeeapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by EPDCL on 21/09/2015.
 */

public class Splashscreen extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newsplash);
        Runnable endSplash = new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(Splashscreen.this, Signin.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        };
        new Handler().postDelayed(endSplash, 5000);
    }
}

