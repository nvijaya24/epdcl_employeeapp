package com.epdcl.employeeapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by EPDCL on 18/09/2015.
 */
public class Activities extends Fragment {

    CategoriesListAdapterChat listAdapter;
    ArrayList<Integer> icon=new ArrayList<Integer>();
    ArrayList<String> title=new ArrayList<String>();
    ListView lstvw;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView =inflater.inflate(R.layout.activities,container,false);
        lstvw=(ListView)rootView.findViewById(R.id.listvw);

        setListViewHeightBasedOnChildren(lstvw);

        icon=new ArrayList<Integer>();
        icon.add(R.drawable.reading);
        icon.add(R.drawable.tester_reading);
        icon.add(R.drawable.maintenance);
        icon.add(R.drawable.aadhaar_collector);

        title=new ArrayList<String>();
        title.add("Check Reading ");
        title.add("DTR Tang Tester Reading");
        title.add("DTR Maintenance");
        title.add("Aadhar Collector");
        listAdapter = new CategoriesListAdapterChat(Activities.this.getActivity(),icon,title);
        lstvw.setAdapter(listAdapter);

        lstvw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(getActivity(), "position-->" + position, Toast.LENGTH_SHORT).show();

            }
        });
        return rootView;
    }
    class CategoriesListAdapterChat extends BaseAdapter
    {
        private Context mContext;
        ArrayList<Integer> data;
        ArrayList<String> data2;
        public CategoriesListAdapterChat(Context context,ArrayList<Integer> data,ArrayList<String> data2)
        {
            super();
            mContext=context;
            this.data=data;
            this.data2=data2;
        }
        public int getCount()
        {
            return data.size();
        }
        // getView method is called for each item of ListView
        public View getView(final int position,  View view, ViewGroup parent)
        {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.activities_list, null);
            ImageView val=(ImageView)view.findViewById(R.id.icon);
            TextView title=(TextView)view.findViewById(R.id.itemtxt);
            title.setText(data2.get(position));
            val.setBackgroundResource(data.get(position));
            return view;
        }
        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
