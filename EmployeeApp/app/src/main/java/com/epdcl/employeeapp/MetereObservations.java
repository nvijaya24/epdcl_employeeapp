package com.epdcl.employeeapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by EPDCL on 22/09/2015.
 */

public class MetereObservations extends Activity {

    ArrayList<String> title=new ArrayList<String>();
    ArrayList<String> count1=new ArrayList<String>();
    ArrayList<String> count2=new ArrayList<String>();
    ListView lstvw;
    TextView txtTitle;
    ImageView back;
    CategoriesListAdapterChat listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meter);
        lstvw=(ListView)findViewById(R.id.listvw);
        txtTitle=(TextView)findViewById(R.id.itemTitle);
        txtTitle.setText("Meter Observations");
        back=(ImageView)findViewById(R.id.back);
        setListViewHeightBasedOnChildren(lstvw);

        title.add("Meter Management");
        title.add("Meter Burnt");
        title.add("UDC(>4Months)");

        count1.add("123");
        count1.add("12");
        count1.add("44");

        count2.add("66");
        count2.add("34");
        count2.add("201");

        listAdapter = new CategoriesListAdapterChat(MetereObservations.this, title,count1,count2);
        lstvw.setAdapter(listAdapter);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        lstvw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(MetereObservations.this, title.get(position), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MetereObservations.this, TableLayoutDisplay.class);
                startActivity(intent);
            }
        });
    }

    class CategoriesListAdapterChat extends BaseAdapter
    {
        private Context mContext;
        ArrayList<String> data;
        ArrayList<String> count1;
        ArrayList<String> count2;
        public CategoriesListAdapterChat(Context context,ArrayList<String> data,ArrayList<String> count1,ArrayList<String> count2)
        {
            super();
            mContext=context;
            this.data=data;
            this.count1=count1;
            this.count2=count2;
        }
        public int getCount()
        {
            return data.size();
        }
        // getView method is called for each item of ListView
        public View getView(final int position,  View view, ViewGroup parent)
        {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.meter_list, null);
            TextView title=(TextView)view.findViewById(R.id.itemtxt);
            TextView tvCount1=(TextView)view.findViewById(R.id.itemCount1);
            TextView tvCount2=(TextView)view.findViewById(R.id.itemCount2);
            title.setText(data.get(position));
            tvCount1.setText(count1.get(position));
            tvCount2.setText(count2.get(position));
            return view;
        }
        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }
    }
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}